# Knowledge Keeper
This project is to manage all kind of information that come up in a life time. For example, code snippets, urls, notes you name it.

## Design
This should be platform independent. For that the storage for the information are text files with Markdown syntax. So, they are readable by humans and can be altered without any program except a text editor. To share this files git or another syncing tool can be used.

At the moment two front ends are plannded. First a command line script that provide commands to add, search, edit and delete notes. Second a webpage provides a GUI to add, search, edit and delete notes as well.
