#!/usr/bin/env raku
use Temp::Path;
use KnowledgeKeeper::DataInterface::File;

# Allow named parameter anywhere to get the unix feeling of commands like
# k2.p6 add --title ...
my %*SUB-MAIN-OPTS = :named-anywhere,
                     ;

# This is the entrypoint for Dependency Injection.
# Will be outsourced into a module for sharing with the web page later.
my $file-path = $*PROGRAM-NAME.IO.parent.parent.add("data");

my $interface = KnowledgeKeeper::DataInterface::File.new(:$file-path);

multi sub MAIN('search', :$needle) {
    my @notes = $interface.GetAllTitles();

    for @notes -> $note {
        say ($note ~~ /(.*?) '.md' /)[0].Str;
    }
}

#| Add note on the command line
multi sub MAIN('add', :$title!, :$data!, :@tags) {
    my $note = KnowledgeKeeper::Note.new(:$title, :$data, :@tags);
    $interface.WriteData($note);
}

#| Add note with your $EDITOR
multi sub MAIN('add', :$title!, :@tags) {
    temp $*TMPDIR = make-temp-path;

    run %*ENV<EDITOR>, $*TMPDIR.Str;

    do { say "Nothing to store!"; exit } if not $*TMPDIR.e;
    my $data = $*TMPDIR.slurp;

    my $note = KnowledgeKeeper::Note.new(:$title, :$data, :@tags);
    $interface.WriteData($note);
}
