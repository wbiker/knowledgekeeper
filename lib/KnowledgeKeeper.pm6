unit module KnowledgeKeeper;

# Independency Injection entry point.
# Here is the place where all configuration are read and set plus the objects created.

use KnowledgeKeeper::DataInterface::File;

my $data-store = KnowledgeKeeper::DataInterface::File.new(file-path => "/home/wolf/repos/KnowledgeKeeper/data");