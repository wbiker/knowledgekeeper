use KnowledgeKeeper::Role;
use KnowledgeKeeper::Note;
use KnowledgeKeeper::Grammar::File;

class KnowledgeKeeper::DataInterface::File does KnowledgeKeeper::Role::DataInterface {
    has IO::Path $.file-path is required;
    has @!cache;

    submethod TWEAK() {
        $!file-path.mkdir if not $!file-path.e;
    }

    method GetAllTitles( --> Array) {
        return (gather for $!file-path.dir(test => /md$/) -> $file {
            take $file.basename;
        }).Array
    }

    method GetByTag($tag) {
        my @all-notes = self!ReadAllData();

        return @all-notes.grep({$_.tags.grep({$_ eq $tag})});
    }

    method GetByTitle($title) {
        my @all-notes = self!ReadAllData();

        return @all-notes.grep(*.title eq $title);
    }

    method WriteData(KnowledgeKeeper::Note $note) {
        my $file-path = $!file-path.add($note.title ~ ".md");

        my $meta = self!GetMetaDataStr($note);
        $file-path.spurt($meta);

        $file-path.spurt($note.data, :append);
    }

    method ReadData(IO::Path $file --> KnowledgeKeeper::Note) {
        my $file-content = $file.slurp;

        my $note = KnowledgeKeeper::Grammar::File.parse($file-content, actions => KnowledgeKeeper::Action::File.new).made;

        $note = KnowledgeKeeper::Note.new(title => "", data => "") unless $note;
        return $note;
    }

    method !GetMetaDataStr(KnowledgeKeeper::Note $note) {
        my $meta = qq:to/METAEND/;
        ---
        title: {$note.title}
        METAEND

        if $note.tags.elems > 0 {
            $meta ~= "tags: [{$note.tags.join(',')}]\n";
        }

        if $note.attachments {
            $meta ~= "attachments: [{$note.attachments.join(',')}]\n";
        }

        $meta ~= "creation date: {$note.creation-date}\n";
        $meta ~= "modification date: {$note.modification-date}\n";

        $meta ~= "---\n";

        return $meta;
    }

    method !ReadAllData() {
        @!cache = (gather for $!file-path.dir(test => /md$/) -> $file {
            take self.ReadData($file);
        }).Array
    }
}