use KnowledgeKeeper::Note;

grammar KnowledgeKeeper::Grammar::File {
    token TOP { <metastart> \n <title> \n <tags>? \n? <creation-date> \n <modification-date> \n <metaend> \n <data> }

    token metastart {
        <meta>
    }

    token title {
	'title: ' <value>
    }

    token tags {
	'tags: [' $<tags>=<-[\]]>+ ']'
    }

    token creation-date {
	'creation date: ' <value>
    }

    token modification-date {
	'modification date: ' <value>
    }

    token metaend {
	<meta>
    }

    token meta {
	'---'
    }

    token value {
	<-[\n]>+
    }

    token tagvalue {
	<-[\]]>+
    }

    token data {
	.*
    }
}

class KnowledgeKeeper::Action::File {
    has $.titleStr;
    has $.tags;

    method TOP($match) {
        my $title = ~$match<title>.made;
        my @tags;
        @tags = $match<tags>.made.Str.split(',')>>.trim if $match<tags>;
        my $creation-date = ~$match<creation-date>.made;
        my $modification-date = ~$match<modification-date>.made;
        my $data = ~$match<data>.made;

        $match.make: KnowledgeKeeper::Note.new(:$title, :@tags, creation-date => DateTime.new($creation-date), modification-date => DateTime.new($modification-date), :$data);
    }

    method title($/) {
        $/.make: $<value>;
    }

    method tags($/) {
        $/.make: $<tags>;
    }

    method creation-date($/) {
        $/.make: $<value>;
    }

    method modification-date($/) {
        $/.make: $<value>;
    }

    method value($/) {
        $/.make: $/;
    }

    method tagvalue($/) {
        $/.make: $/;
    }

    method data($/) {
        $/.make: $/;
    }
}