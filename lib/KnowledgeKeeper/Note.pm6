class KnowledgeKeeper::Note {
    has $.title is required;
    has $.data is required;
    has @.tag;
    has @.attachments;
    has DateTime $.creation-date = DateTime.now;
    has DateTime $.modification-date = DateTime.now;
}
