role KnowledgeKeeper::Role::DataInterface {
    method GetAllTitles() {...}
    method GetByTag($tag) {...}
    method GetByTitle($title) {...}
}