use Cro::HTTP::Router;
use Cro::WebApp::Template;

sub routes() is export {
    route {
        get -> {
            content 'text/html', "<h1> KnowledgeKeeper </h1>";
        }

        get -> 'greet', $name {
            template 'templates/greet.crotmp', { :$name }
        }
    }
}