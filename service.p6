use Cro::HTTP::Log::File;
use Cro::HTTP::Server;
use KnowledgeKeeper::Routes;

my Cro::Service $http = Cro::HTTP::Server.new(
    http => <1.1>,
    host => %*ENV<KNOWLEDGEKEEPER_HOST> ||
        die("Missing KNOWLEDGEKEEPER_HOST in environment"),
    port => %*ENV<KNOWLEDGEKEEPER_PORT> ||
        die("Missing KNOWLEDGEKEEPER_PORT in environment"),
    application => routes(),
    after => [
        Cro::HTTP::Log::File.new(logs => $*OUT, errors => $*ERR)
    ]
);
$http.start;
say "Listening at http://%*ENV<KNOWLEDGEKEEPER_HOST>:%*ENV<KNOWLEDGEKEEPER_PORT>";
react {
    whenever signal(SIGINT) {
        say "Shutting down...";
        $http.stop;
        done;
    }
}
