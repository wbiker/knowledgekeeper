use Test;
use Temp::Path;

use KnowledgeKeeper::DataInterface::File;
use KnowledgeKeeper::Note;

with make-temp-path {
    my $file-path = $_;

    # KnowledgeKeeper::DataInterface::File should create the given file path when not already exists.
    my $dif = KnowledgeKeeper::DataInterface::File.new(file-path => $file-path);
    is $file-path.e, True, "Data file path created";

    # KnowledgeKeeper::DataInterface::File::GetAllTitles()
    # Create file not used for storage, thus should not be returned.
    my $ignored-file = $file-path.add("first test file");
    # Create file in the file system.
    $ignored-file.spurt("");
    # Create valid storage file, which is expected to be returned.
    my $valid-file = $file-path.add("2nd Ö file.md");
    $valid-file.spurt("");

    my @all-titles = $dif.GetAllTitles();
    is @all-titles.elems, 1, "GetAllTitle() just returns files with md extension";
    is @all-titles[0], "2nd Ö file.md", "GetAllTtitle() returns the proper file with md extension";

    $file-path.add("anotherstoragefile.md").spurt("");
    my @more-storage-files = $dif.GetAllTitles();
    is @more-storage-files.elems, 2, "GetAllTitle() returns more than one storage file";
    is @more-storage-files[0], "anotherstoragefile.md",
    "GetAllTitle() returns the proper file for more than one storage file";

    # KnowledgeKeeper::DataInterface::File::WriteData()
    {
        my $note = KnowledgeKeeper::Note.new(title => "note test", data => "# note test\ntest content");
        $dif.WriteData($note);

        my $note-meta-data = qq:to/END/;
        ---
        title: note test
        creation date: {$note.creation-date}
        modification date: {$note.modification-date}
        ---
        # note test
        test content
        END
        $note-meta-data .= trim;

        my $expected-file = $file-path.add("note test.md");
        my $test-file-content = $expected-file.slurp;
        is $test-file-content, $note-meta-data, "WriteData() writes notes data into given file";

    # KnowledgeKeeper::DataInterface::File::ReadData()
        my $test = $file-path.add("tmp.md");
        $test.spurt($note-meta-data);
        my $actual-note = $dif.ReadData($test);
        isa-ok $actual-note, KnowledgeKeeper::Note, "ReadData() returns Note";
        is $actual-note.title, "note test", "ReadData() returns proper Note title";
    }

    # KnowledgeKeeper::DataInterface::File::GetByTag()
    {
        my $note-meta-data = qq:to/END/;
        ---
        title: tag test
        tags: [test,more tests]
        creation date: {DateTime.now}
        modification date: {DateTime.now}
        ---
        # note test
        test content
        END
        $note-meta-data .= trim;

        $file-path.add("tag test.md").spurt($note-meta-data);
        my @tags = $dif.GetByTag("test");
        is @tags.elems, 1, "GetByTag() found note with tag'";
        is @tags[0].title, "tag test", "GetByTag() found proper note";
    }

    {
        my $note-meta-data = qq:to/END/;
        ---
        title: tag with spaces test
        tags: [tag with spaces]
        creation date: {DateTime.now}
        modification date: {DateTime.now}
        ---
        # note test
        test content
        END
        $note-meta-data .= trim;

        $file-path.add("tag with spaces test.md").spurt($note-meta-data);
        my @tags = $dif.GetByTag("tag with spaces");
        is @tags.elems, 1, "GetByTag() found correct amount of notes";
        is @tags[0].title, "tag with spaces test", "GetByTag() found note with a tag with spaces";
        is-deeply @tags[0].tags, ["tag with spaces"], "GetByTag() returns note with tag with spaces";
    }

    # KnowledgeKeeper::DataInterface::File::GetByTitle()
    {
        my $note-meta-data = qq:to/END/;
        ---
        title: get title test
        creation date: {DateTime.now}
        modification date: {DateTime.now}
        ---
        # note test
        test content
        END
        $note-meta-data .= trim;

        $file-path.add("getbytitletest.md").spurt($note-meta-data);
        my @titles = $dif.GetByTitle("get title test");
        is @titles.elems, 1, "GetByTitle() returned expected amount of notes";
        is @titles[0].title, "get title test", "GetByTitle() found note with given title";
    }
}

done-testing;
