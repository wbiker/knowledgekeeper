use Test;
use lib 'lib';

use KnowledgeKeeper::Grammar::File;

my $actions = KnowledgeKeeper::Action::File.new;

{
    my $note-meta-data = qq:to/END/;
    ---
    title: test file title
    creation date: 2020-06-14T16:04:59.044498+02:00
    modification date: 2020-06-25T10:26:40.452239+02:00
    ---
    Test file content
    END
    $note-meta-data .= trim;

    my $match = KnowledgeKeeper::Grammar::File.parse($note-meta-data);

    is $match<title><value>, "test file title", "Right title found";
    is $match<tags>.defined, False, "No tags found";
    is $match<creation-date><value>, "2020-06-14T16:04:59.044498+02:00", "creation date is the proper one";
    is $match<modification-date><value>, "2020-06-25T10:26:40.452239+02:00", "modification-date found";
    is $match<data>, "Test file content", "Data found";
}

{
    my $note-meta-data = qq:to/END/;
    ---
    title: test file title
    tags: [tag,tag1,tag2,tag 3]
    creation date: 2020-06-14T16:04:59.044498+02:00
    modification date: 2020-06-25T10:26:40.452239+02:00
    ---
    Test file content
    END
    $note-meta-data .= trim;

    my $tag-match = KnowledgeKeeper::Grammar::File.parse($note-meta-data);
    is $tag-match<tags><tags>, "tag,tag1,tag2,tag 3", "Tags found";
}

{
    my $note-meta-data = qq:to/END/;
    ---
    title: action test title
    tags: [action,action1,action2,action 3]
    creation date: 2020-06-25T13:51:34.650654+02:00
    modification date: 2020-06-25T13:51:53.833818+02:00
    ---
    Action test file content
    END
    $note-meta-data .= trim;

    my $note = KnowledgeKeeper::Grammar::File.parse($note-meta-data, :$actions).made;
    isa-ok $note, KnowledgeKeeper::Note, "Action returns Note";
    is $note.title, "action test title", "Action has found title";
    is-deeply $note.tags, ["action", "action1", "action2", "action 3"], "Action has found tags";
    is $note.creation-date.Str, "2020-06-25T13:51:34.650654+02:00", "Action has found the correct creation date";
    is $note.modification-date.Str, "2020-06-25T13:51:53.833818+02:00", "Action has found the correct creation date";
    is $note.data, "Action test file content", "Action found data";
}

{
    my $note-meta-data = qq:to/END/;
    ---
    title: no tags test
    creation date: 2020-06-25T13:51:34.650654+02:00
    modification date: 2020-06-25T13:51:53.833818+02:00
    ---
    data
    END
    $note-meta-data .= trim;

    my $note = KnowledgeKeeper::Grammar::File.parse($note-meta-data, :$actions).made;
    is $note.tags, [], "Note without tags does not fail";
}

{
    my $test = qq:to/END/;
    ---
    creation date: 2020-06-14T16:04:59.044498+02:00
    modification date: 2020-06-14T16:04:59.044498+02:00
    ---
    Test file content
    END

    my $note = KnowledgeKeeper::Grammar::File.parse($test, :$actions).made;
    isa-ok $note, Any, "Missing title fails to parse grammar";
}

{
    my $note = KnowledgeKeeper::Grammar::File.parse(
    "---\ntitle: note test\ncreation date: 2020-06-22T15:44:27.000630+02:00\nmodification date: 2020-06-22T15:44:27.001360+02:00\n---\n# note test\ntest content\n",
    :$actions).made;
    is $note.title, "note test", "Title found in string";
}

done-testing;
