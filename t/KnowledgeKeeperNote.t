use Test;
use lib 'lib';

use KnowledgeKeeper::Note;

my $note = KnowledgeKeeper::Note.new(title => "title", data => "data");
is $note.title, "title", "Note title found";
is $note.creation-date.defined, True, "Creation date created upon creation";
is $note.modification-date.defined, True, "Modification date created upon creation";
is $note.data, "data", "Data found";

done-testing;
