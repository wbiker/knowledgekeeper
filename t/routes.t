use Cro::HTTP::Test;
use Test;
use KnowledgeKeeper::Routes;

test-service routes, {
    test get('/'),
            status => 200,
            body-text => '<h1> KnowledgeKeeper </h1>';
}

done-testing;
